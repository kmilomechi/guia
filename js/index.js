$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 5000
    });

    $('#contacto').on('show.bs.modal', function () {
        console.log("el modal se esta mostrando");
        $("#contactoBTN").removeClass("btn-outline-sucess");
        $("#contactoBTN").addClass("btn-primary");
        $("#contactoBTN").prop("disabled", true);
    });
    $('#contacto').on('hide.bs.modal', function () {
        console.log("el modal se esta ocultando");
        $("#contactoBTN").removeClass("btn-primary");
        $("#contactoBTN").addClass("btn-outline-sucess");
        $("#contactoBTN").prop("disabled", false);
    });
});